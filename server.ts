import { Application, Context, Router } from "https://deno.land/x/oak@v6.3.2/mod.ts";
import {
  WebSocketMiddleware,
  WebSocketMiddlewareHandler, 
} from "https://raw.githubusercontent.com/jcc10/oak_websoket_middleware/v1.1.1/mod.ts";
// deno-lint-ignore camelcase
import { BCC_Middleware } from "https://raw.githubusercontent.com/jcc10/oak_bundle-compile-cache_middleware/main/mod.ts";
import { WEBSOCKET_SIGNAL_SERVER } from "./websocket_signal_server.ts";
import { GLOBAL_ECHO_SERVER } from "./global_echo_server.ts";

const app = new Application();
const wsApp = new WebSocketMiddlewareHandler();
app.use(WebSocketMiddleware(wsApp.handle()));
const signalServer = new WEBSOCKET_SIGNAL_SERVER("/rtcSignals");
wsApp.use(signalServer.socket_handler());
const globalEcho = new GLOBAL_ECHO_SERVER();
wsApp.use(globalEcho.handler());

const bccMiddle = new BCC_Middleware({
  BCC_Settings: {
    tsSource: "client-ts",
    bundleFolder: undefined,
    compiledFolder: "compiled",
    cacheFolder: undefined,
    cacheRoot: undefined,
    mapSources: false,
  },
});
// Clear old files.
await bccMiddle.bcc.clearAllCache();
await Promise.all([
  bccMiddle.bcc.compile("./WebRTCManager.ts"),
  bccMiddle.bcc.compile("./wsEcho.ts"),
  bccMiddle.bcc.compile("./rtcConn.ts"),
]);
// Load the middleware.
app.use(bccMiddle.middleware());

const router = new Router();
app.use(router.routes());
app.use(router.allowedMethods());

// This is for the actual HTML pages.
router.get("/localConn.html", async (context: Context) => {
  context.response.body = await Deno.readTextFile(
    `${Deno.cwd()}/static/localConn.html`,
  );
  context.response.type = "text/html";
}).get("/index.html", async (context: Context) => {
  context.response.body = await Deno.readTextFile(
    `${Deno.cwd()}/static/index.html`,
  );
  context.response.type = "text/html";
}).get("/", async (context: Context) => {
  context.response.body = await Deno.readTextFile(
    `${Deno.cwd()}/static/index.html`,
  );
  context.response.type = "text/html";
});
const appPromise = app.listen({ port: 3000 });
console.log("Server running on localhost:3000");