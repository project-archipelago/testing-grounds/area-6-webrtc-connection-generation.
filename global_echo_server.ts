import { wsHandler } from "https://raw.githubusercontent.com/jcc10/oak_websoket_middleware/v1.1.1/mod.ts";import {
  isWebSocketCloseEvent,
  isWebSocketPingEvent,
  WebSocket,
} from "https://deno.land/std@0.77.0/ws/mod.ts";
import { v4 } from "https://deno.land/std@0.77.0/uuid/mod.ts";
export class GLOBAL_ECHO_SERVER {
    private users: Map<string, WebSocket> = new Map<string, WebSocket>();
    private async socketHandler(socket: WebSocket, url: URL, headers: Headers){
        const userId = v4.generate();

        // Register user connection
        this.users.set(userId, socket);
        // Wait for new messages
        try {
        for await (const ev of socket) {
            if (typeof ev === "string") {
                // text message
                if (isWebSocketCloseEvent(ev)) {
                    this.users.delete(userId);
                    this.broadcast(`${userId} is disconnected`);
                    break;
                }
                const message = (typeof ev === "string" ? ev : "");
                this.broadcast(`WS: [${message}]`);
            } else if (ev instanceof Uint8Array) {
                //stub
            } else if (isWebSocketPingEvent(ev)) {
                const [, body] = ev;
                // ping
                // stub
                //console.log("ws:Ping", body);
            } else if (isWebSocketCloseEvent(ev)) {
                // close
                const { code, reason } = ev;
                //console.log("ws:Close", code, reason);
            }
        }
        } catch (err) {
        console.error(`failed to receive frame: ${err}`);

            if (!socket.isClosed) {
                await socket.close(99).catch(console.error);
            }
        }
        this.users.delete(userId);
    }
    public handler(): wsHandler {
        return async (socket: WebSocket, url: URL, headers: Headers) => {
            await this.socketHandler(socket, url, headers);
        }
    }

    private async broadcast(message: string): Promise<void> {
        if (!message) return;
        for (const user of this.users.values()) {
        if (user.isClosed) {
            continue;
        }
        await user.send(message);
        }
    }
}