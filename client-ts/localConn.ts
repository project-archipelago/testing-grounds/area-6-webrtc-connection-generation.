/// <reference lib="esNext" />
/// <reference lib="dom" />
/// <reference lib="webworker.importScripts" />
/// <reference lib="ScriptHost" />
/// <reference lib="dom.iterable" />
/// <reference no-default-lib="true"/>

/**
 * Note: All the <class> signatures are because the script does not lint properly without it.
 */

let connectButton: HTMLButtonElement;
let disconnectButton: HTMLButtonElement;
let sendButton: HTMLButtonElement;
let messageInputBox: HTMLInputElement;
let receiveBox: HTMLDivElement;
let localConnection: RTCPeerConnection|null ; // RTCPeerConnection for our "local" connection
let remoteConnection: RTCPeerConnection|null ; // RTCPeerConnection for the "remote"

let sendChannel:RTCDataChannel|null ; // RTCDataChannel for the local (sender)
let receiveChannel:RTCDataChannel|null ; // RTCDataChannel for the remote (receiver)

function startup() {
  connectButton = <HTMLButtonElement> document.getElementById("connectButton");
  disconnectButton = <HTMLButtonElement>document.getElementById("disconnectButton");
  sendButton = <HTMLButtonElement>document.getElementById("sendButton");
  messageInputBox = <HTMLInputElement>document.getElementById("message");
  receiveBox = <HTMLDivElement>document.getElementById("receivebox");

  // Set event listeners for user interface widgets

  connectButton.addEventListener("click", connectPeers, false);
  disconnectButton.addEventListener("click", disconnectPeers, false);
  sendButton.addEventListener("click", sendMessage, false);
}

async function connectPeers() {
  // Create the local connection and its event listeners

  localConnection = <RTCPeerConnection>new RTCPeerConnection();

  // Create the data channel and establish its event listeners
  sendChannel = localConnection.createDataChannel("sendChannel");
  sendChannel.onopen = handleSendChannelStatusChange;
  sendChannel.onclose = handleSendChannelStatusChange;

  // Create the remote connection and its event listeners

  remoteConnection = <RTCPeerConnection>new RTCPeerConnection();
  remoteConnection.ondatachannel = receiveChannelCallback;

  // Set up the ICE candidates for the two peers

  localConnection.onicecandidate = (e) => {
    try {
      !e.candidate ||
      remoteConnection?.addIceCandidate(e.candidate);
    } catch (e) {
      handleAddCandidateError(e);
    }
  }

  remoteConnection.onicecandidate = (e) => {
    try {
      !e.candidate ||
      localConnection?.addIceCandidate(e.candidate);
    } catch (e) {
      handleAddCandidateError(e);
    }
  }

  // Now create an offer to connect; this starts the process

  try{

    const offer = await localConnection.createOffer();
    await localConnection.setLocalDescription(offer);
    await remoteConnection.setRemoteDescription(offer);
    const answer = await remoteConnection.createAnswer();
    await remoteConnection.setLocalDescription(answer);
    await localConnection.setRemoteDescription(answer);
  } catch (e){
    handleCreateDescriptionError(e);
  }
}

// Handle errors attempting to create a description;
// this can happen both when creating an offer and when
// creating an answer. In this simple example, we handle
// both the same way.

function handleCreateDescriptionError(error: Error) {
  console.log("Unable to create an offer: " + error.toString());
}

// Handle successful addition of the ICE candidate
// on the "local" end of the connection.

function handleLocalAddCandidateSuccess() {
  connectButton.disabled = true;
}

// Handle successful addition of the ICE candidate
// on the "remote" end of the connection.

function handleRemoteAddCandidateSuccess() {
  disconnectButton.disabled = false;
}

// Handle an error that occurs during addition of ICE candidate.

function handleAddCandidateError(e: Error) {
  console.log("Oh noes! addICECandidate failed!");
}

// Handles clicks on the "Send" button by transmitting
// a message to the remote peer.

function sendMessage() {
  var message = messageInputBox.value;
  sendChannel?.send(message);

  // Clear the input box and re-focus it, so that we're
  // ready for the next message.

  messageInputBox.value = "";
  messageInputBox.focus();
}

// Handle status changes on the local end of the data
// channel; this is the end doing the sending of data
// in this example.

function handleSendChannelStatusChange(event: Event) {
  let state:RTCDataChannelState;
  if (sendChannel) {
    state = sendChannel.readyState;

    if (state === "open") {
      messageInputBox.disabled = false;
      messageInputBox.focus();
      sendButton.disabled = false;
      disconnectButton.disabled = false;
      connectButton.disabled = true;
    } else {
      messageInputBox.disabled = true;
      sendButton.disabled = true;
      connectButton.disabled = false;
      disconnectButton.disabled = true;
    }
  }
}

// Called when the connection opens and the data
// channel is ready to be connected to the remote.

function receiveChannelCallback(event: RTCDataChannelEvent) {
  receiveChannel = event.channel;
  receiveChannel.onmessage = handleReceiveMessage;
  receiveChannel.onopen = handleReceiveChannelStatusChange;
  receiveChannel.onclose = handleReceiveChannelStatusChange;
}

// Handle onmessage events for the receiving channel.
// These are the data messages sent by the sending channel.

function handleReceiveMessage(event: MessageEvent) {
  var el = <HTMLParagraphElement>document.createElement("p");
  var txtNode = <Text>document.createTextNode(event.data);

  el.appendChild(txtNode);
  receiveBox.appendChild(el);
}

// Handle status changes on the receiver's channel.

function handleReceiveChannelStatusChange(event: Event) {
  if (receiveChannel) {
    console.log(
      "Receive channel's status has changed to " +
        receiveChannel.readyState,
    );
  }

  // Here you would do stuff that needs to be done
  // when the channel's status changes.
}

// Close the connection, including data channels if they're open.
// Also update the UI to reflect the disconnected status.

function disconnectPeers() {
  // Close the RTCDataChannels if they're open.

  sendChannel?.close();
  receiveChannel?.close();

  // Close the RTCPeerConnections

  localConnection?.close();
  remoteConnection?.close();

  sendChannel = null;
  receiveChannel = null;
  localConnection = null;
  remoteConnection = null;

  // Update user interface elements

  connectButton.disabled = false;
  disconnectButton.disabled = true;
  sendButton.disabled = true;

  messageInputBox.value = "";
  messageInputBox.disabled = true;
}

window.addEventListener("load", startup, false);

// This is here b/c of Deno Issue #8684
export {};