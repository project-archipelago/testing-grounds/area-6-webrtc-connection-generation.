/// <reference lib="esNext" />
/// <reference lib="dom" />
/// <reference lib="webworker.importScripts" />
/// <reference lib="ScriptHost" />
/// <reference lib="dom.iterable" />
/// <reference no-default-lib="true"/>
const sendButton: HTMLButtonElement = <HTMLButtonElement> document.getElementById("sendWS");
const messageInputBox: HTMLInputElement = <HTMLInputElement> document.getElementById("message");
const receiveBox: HTMLDivElement = <HTMLDivElement> document.getElementById("receivebox");
const ws = new WebSocket(`ws://${window.location.host}/echo`);

ws.onmessage = (ev:MessageEvent)=>{
    var el = <HTMLParagraphElement> document.createElement("p");
    var txtNode = <Text> document.createTextNode(ev.data);

    el.appendChild(txtNode);
    receiveBox.appendChild(el);
};

function sendMessage(){
    var message = messageInputBox.value;
    ws.send(message)
}

sendButton.addEventListener("click", sendMessage, false);

export {};